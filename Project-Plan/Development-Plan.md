# iinvest Project Plan

---

This document defines a proposed technical development plan of the iinvest group. This document will outline various projected deadlines particular to the implementation and deployment of various backend services. 

## Who We Are
iinvest is a technological platform founded on the belief that a future where investing is simplified is fundamentally more exciting than one where it isn’t. We are attempting to provide a consolidated socially interactive financial platform to serve as a one stop shop for one’s financial needs. We recognize that the most robust solution to the modern investment problem is a sleek distributed network that provides users with attractive learning resources, up to date and credible market news, simplified market summaries, a focalized financial research environment, and the ability to leverage your network as you make financial decisions. As the saying “think outside the box” goes, we decided to construct this unorthodox solution in hopes of disentangling the various complexities associated with finance, hence, simplifying the time-intensive effort attributed to consuming compacted market data on the behalf of our users, and creating a future comprised of more financially adept individuals.

---

## Technical Solution
The iinvest application will be comprised of a multitude of backend microservice. The primary application and benefit of such an implementation pattern
is the enforcement of loose coupling of service logic and well as technical organizational independence. 

The backend architecture will be comprised of various __go__ and __java__ microservices. These will allow for a high performant backend system able to serve the required business use cases and the proposed problem.
For truly computing intensive low latency use cases, __C++__ will also be utilized. 
__Java__ will be applied for big data processing based services as well as services implementing streaming. Additionally, the proposed recommendation service will be implemented in __Java__.

---
##  Service Definitions
Present below are the required backend services and their respective service definitions.
- Financial Data Microservice   
    - Microservice tasked with interfacing with the iex trading exchange's service api to obtain investment data on the behalf of client requests.
- Portfolio Microservice
    - Microservice tasked with providing portfolio and watchlist metrics to users. Additionally microservice will apply all portfolio related functions such as
    stress testing, risk/exposure analysis, and projections
    - Support for the following Portfolio Functionalities
        - Stress Testing
            - Expected Loss Under Various Scenarios
        - Machine Learning Predictive Services
        - Risk
            - Sector Exposure
            - Beta to SPY
            - Pos. Concentration
            - Net. Dollar Exposure
            - Turnover
        - Performance Analysis
            - Total Returns
            - Specific Returns
            - Common Returns
            - Sharpe 
            - Volatility
            - Max. Drawdown 
- Brokerage Microservice
    - Microservice tasked with enabling access to a user's brokerage account through sdk's and api calls. Additionally, the service will be tasked with 
    properly storing credentials.
        - Support for the following APIs
            - E-trade API
            - Robinhood API
            - TD Ameritrade API
- News Microservice
    - Microservice tasked with obtaining news articles. Additionally, this service will support the following features
        - Saved News Articles
        - Add News Sources/RSS & Follow Feeds
        - Provide Users With Blog Access
        - Follow Favorite Twitter Influencer & Never Miss Updates
        - Mark Articles As Read/UnRead
        - Follow & Discover Topics From Any Source
        - model after __feedly.com__ in terms of feature sets and __bynder__ from a UI perspective
- Chat Microservice
    - Microservice tasked with implementing basic chat functionalities
    - Features include the following
        - Direct Messages
        - Private Groups & Public Groups
        - Notifications
        - Avatars
        - Emojies
        - Reactions
        - Media Embedding
        - Link Previews
        - File Upload/Sharing
        - Full text search
- Podcast Microservice
    - Microservice tasked with providing podcast services. A bit vague but sample features include the following
        - Search Podcasts 
            - title, author, category
        - Listen To Podcasts
        - Podcast Pane
            - About Podcasts
            - Podcast Feed
            - Podcast Reviews (Thumbs Up/Down)
        - Discover Podcasts
        - Follow Podcasts
        - Podcasts In Progress
        - Podcast New Releases
        - Model After https://github.com/Zimniros/Podcast-Tune-V2
        and https://github.com/Zimniros/PodcastTune __PodcastTune__
- Learn Microservice
    - Microservice tasked with interfacing with khan academy api and various other educational resources to provide a learning mechanism for users
        - Support for the following features must be present
            - Access To khan academy finance data repository
            - Access To Youtube finance data repository (learning)
            - Follow Educational Topics & Gamify the learning process
- Research Microservice
    - Microservice tasked with providing publications, journal, and various other research material to user
        - Support for the following sets of features is required
            - Search functionality
                - Simple & Advanced
            - Save/Bookmark Publication To Read Later
            - View Publications in Browser
            - Subscribe To Particular Categories, Keywords, Authors
            - Publication/Journal Recommendations
- Email Microservice
    - Microservice tasked with sending login, registration, password change, newsletter emails
- News-Letter Microservice
    - Microservice tasked with sending newsletters to registered users
- Social Networking Microservice
    - Microservice tasked with implementing the following set of social networking functionalities
        - Post
        - Comment
        - Like/Unlike/Share
        - File Upload & Link Preview
        - Follow & UnFollow
        - User Recommendations
        - Groups
        - User Discovery
        - Hashtags & Mentions
- Publishing Microservice
    - Microservice tasked with implementing a publishing service which will allow users to post articles and publish content on the 
    iinvest platform
        - Publish Article/Publications
        - Upvote/Downvote Articles
        - Share Article To Network
        - Follow Topics & Tie Articles To Topics
        - Comment On Published Content
- API Gateway
    - GRPC gateway/KrakenD gateway microservice
- Database Service
    - Utilize open sourced Vitess As A Database Service 
---
## Project Plan
Summer 2019 Goals
1. Read 
    - 5 Technical Books (Golang, Microservices, ..etc)
    - 2 Business Books (Startups)
    - 2 Life Books
    - Build iinvest company site (HUGO golang framework) Through Use Of Markdown
2. Complete iinvest Company Site
3. Surveys
   - Dish Out 5 Different Surveys To Test Hypothesis
    - Privacy Focused Social Network
    - Financial Analytics
    - Content Control
    - Education
    - User Goals
4. Complete the Following Services
    - Podcast Service
    - News Service
    - Learn Service
    - Research Service
    - News-Letter & Email Service
    
### Important Points To Address
#### Logging

#### Swagger Documentation Generator

#### Continious Integration

#### Best Practices/Docker Workflow

#### Protocol Buffers & GRPC

#### Website
      
