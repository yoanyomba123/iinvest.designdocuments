# iinvest Technical Design Document & Notes

---
This repository contains documents describing the design of the iinvest backend's technical infrastructure. Additionally, it provides various high level overviews of the underlying implementation details of various constructs.

