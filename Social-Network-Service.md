## Social Network Microservice 
---

The social network Microservice is a system comprised of various components primarily aimed at providing a highly scalable 
, fast, and easy to contribute social network.

### Features
---
Present below are the preliminary basic features this service should provide for our end users. More advanced features
will be addressed later in this document. 

Activity Interaction  
  - Posts
  - Comments
  - Likes/Unlikes
  - Sharing
  - Hashtags
Groups Interaction
  - Groups Messaging
  - Group Activity Interaction (like,share, etc...)
  - Group Content Visibility Restriction
Topic Feeds
  - Follow & Unfollow Markets Topic Feeds
  - Realtime news system should post relevant news articles to respective topic feeds
  - Articles -> Topics Relation   
    - Articles should be associated to their respective topics & all following users should attain this in their feed
User Interaction
  - Messages
  - Activity Based Interactions
  - Follow & Unfollow
  - Mentions 
Feeds 
  - Personalized Activity Feeds
  - Personalized Feed/Users Suggestions
  - Notification Feed
Analytics
  - Activity Engagement
  - User Activity Performance Tracking
  - Impressions/Engagement Score


## Project Sprints
---
Primary Sprint (1 Month)
  - Efficient Web Server Development
    - FastHTTP
      - logging
      - documentation (Swagger)
      - error handling
      - linting
      - testing
  - User Service Development 
    - Create, Edit, Delete Users
    - List N users
    - List Users Based On Criteria
    - Follow/Unfollow User (Follow Relationship Creation)
    - Block User
    - Get A Specific User (By Criteria)
    - Friend Request (Accept, Deny, Send)
  - Activity
    - Activity Creation & Interaction
    - Posts & Comment Association (Getstream & Neo4J)
    - User Activity Interaction (Likes, Unlikes, Downvote, Upvotes, ...etc)
    - Activity Sharing

